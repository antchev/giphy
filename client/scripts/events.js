/* eslint-disable max-len */

/**
 * Show trending gifs.
 *
 * @param {function} callback User's event handler callback function
 * @return {function} Returns function that binded to the element's event
 */
export const showTrending = (callback) => $(document).on('click', '#trending', (callback));

/**
 * Show uploaded gifs.
 *
 * @param {function} callback User's event handler callback function
 * @return {function} Returns function that binded to the element's event
 */
export const showUpload = (callback) => $(document).on('click', '#upload', (callback));

/**
 * Upload a gif.
 *
 * @param {function} callback User's event handler callback function
 * @return {function} Returns function that binded to the element's event
 */
export const uploadButton = (callback) => $(document).on('click', '#btn-upload', (callback));

/**
 * Search for a gif.
 *
 * @param {function} callback User's event handler callback function
 * @return {function} Returns function that binded to the element's event
 */
export const searchGif = (callback) => $(document).on('click', '#search-btn', (callback));

/**
 * Infinite scrolling.
 *
 * @param {function} callback User's event handler callback function
 * @return {function} Returns function that binded to the element's event
 */
export const infiniteScroll = (callback) => $(window).on('scroll', (callback));

/**
 * Display gif details.
 *
 * @param {function} callback User's event handler callback function
 * @return {function} Returns function that binded to the element's event
 */
export const displayDetails = (callback) => $(document).on('click', '.card-image', callback);

/**
 * Display search suggestions.
 *
 * @param {function} callback User's event handler callback function
 * @return {function} Returns function that binded to the element's event
 */
export const displaySearchSuggestions = (callback) => $(document).on('focus keyup', '#search-term', callback);

/**
 * Add a favorite gif.
 *
 * @param {function} callback User's event handler callback function
 * @return {function} Returns function that binded to the element's event
 */
export const addFavorite = (callback) => $(document).on('click', '.not-liked-click', callback);

/**
 * Remove gif from favorite.
 *
 * @param {function} callback User's event handler callback function
 * @return {function} Returns function that binded to the element's event
 */
export const removeFavorite = (callback) => $(document).on('click', '.liked-click', callback);

/**
 * Copy gif link to clipboard
 *
 * @param {function} callback User's event handler callback function
 * @return {function} Returns function that binded to the element's event
 */
export const copyGifUrlAddress = (callback) => $(document).on('click', '.get-link', callback);
