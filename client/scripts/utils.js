let counter = 0;
let page = 'Search';

/**
 * Prepare upload request.
 *
 * @return {string} formData information of the uploaded gif
 */
export const prepareUploadRequest = () => {
  const fileInput = $('#upload-input');
  const file = fileInput[0].files[0];
  const formData = new FormData();
  formData.append('file', file);
  return formData;
}

/**
 * Get counter.
 *
 * @return {number} returns the count of the start position of the result
 */
export const getCounter = () => {
  return counter;
}

/**
 * Set counter.
 *
 * @param {number} number sets the start position of the result
 */
export const setCounter = (number) => {
  counter = number;
}

/**
 * Reset counter to 0.
 */
export const resetCounter = () => {
  counter = 0;
}

/**
 * Get current page.
 *
 * @return {string} number sets the start position of the result
 */
export const getPage = () => {
  return page;
}

/**
 * Set page name.
 *
 * @param {string} selector sets the name of the page
 */
export const setPage = (selector) => {
  page = selector;
}

/**
 * Resets search name container.
 *
 */
export const resetSearchField = () => {
  $('#search-term').val('');
}

/**
 * Copy gif's link to clipboard.
 *
 * @param {event} event target used to get the ID address and copy URL
 */
export const copyLinkToClipboard = async (event) => {
  try {
    const imageUrl = event.target.id;
    const res = await navigator.clipboard.writeText(imageUrl);
    toastr.success('GIF link copied to clipboard!');
  } catch (error) {
    toastr.error(error.message);
  }
};
