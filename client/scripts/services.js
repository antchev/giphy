import {
  API_KEY,
  SERVER,
  SERVER_UPLOAD,
  LIMIT
} from './constants.js';
import {
  prepareUploadRequest
} from './utils.js';
import {
  setLocalUpload,
  getLocalUploads,
  getLocalFavorite
} from './localStorage.js';

/**
 * Searches by a given search term.
 * Utilizes Giphy API Search Endpoint.
 *
 * @param {function} handler Function which will handle
 * the received {Promise<Array>} containing Giphy API GIF objects
 * and draw them to the interface
 * @param {number} offset Specifies the starting position of the results
 */
const getSearch = async (handler, offset) => {
  const $input = $('#search-term');
  let searchString = $input.val();
  // If no search string is available,
  // generate random letter to pass to the API
  if (searchString === '') {
    const randomChar = String.fromCharCode(97 + Math.floor(Math.random() * 26));
    searchString = randomChar;
  }

  /* eslint-disable max-len */
  const query = `${SERVER}/gifs/search?api_key=${API_KEY}&q=${searchString}&limit=${LIMIT}&offset=${offset}`;
  /* eslint-enable max-len */
  try {
    const promise = await fetch(query);
    const json = await promise.json();
    handler(json.data);
  } catch (error) {
    toastr.error(error.message);
  }
};

/**
 * Gets the most popular gifs.
 * Utilizes Giphy API Trending Endpoint.
 *
 * @param {function} handler Function which will handle
 * the received {Promise<Array>} containing Giphy API GIF objects
 * and draw them to the interface
 * @param {number} offset Specifies the starting position of the results
 */
const getTrending = async (handler, offset) => {
  /* eslint-disable max-len */
  const query = `${SERVER}/gifs/trending?api_key=${API_KEY}&limit=${LIMIT}&offset=${offset}`;
  /* eslint-enable max-len */

  try {
    const promise = await fetch(query);
    const json = await promise.json();
    handler(json.data);
  } catch (error) {
    toastr.error(error.message);
  }
};

/**
 * Gets a random gif.
 * Utilizes Giphy API Random Endpoint.
 *
 * @param {function} handler Function which will handle
 * the received Giphy API GIF object
 * and draw it to the interface
 */
const getRandom = async (handler) => {
  const query = `${SERVER}/gifs/random?api_key=${API_KEY}`;

  try {
    const promise = await fetch(query);
    const json = await promise.json();
    handler(json.data);
  } catch (error) {
    toastr.error(error.message);
  }
};

/**
 * Gets a gif by selected id.
 * Utilizes Giphy API Get GIF by ID Endpoint.
 *
 * @param {string} id The id of the requested gif
 * @param {function} handler Function which will handle
 * the received Giphy API GIF object
 * and draw it to the interface
 */
// eslint-disable-next-line consistent-return
const getGifById = async (id, handler) => {
  const query = `${SERVER}/gifs/${id}?api_key=${API_KEY}&gif_id=${id}`;

  try {
    const promise = await fetch(query);
    const json = await promise.json();
    return handler(json.data);
  } catch (error) {
    toastr.error(error.message);
  }
};

/**
 * Gets a gif by selected id.
 * Utilizes Giphy API Get GIFs by ID Endpoint.
 *
 * @param {string} ids A comma separated string containing the requested IDs
 * @param {function} handler Function which will handle
 * the received {Promise<Array>} containing Giphy API GIF objects
 * and draw it to the interface
 */
const getGifsById = async (ids, handler) => {
  if (ids === null) {
    getLocalFavorite();
  }
  const query = `${SERVER}/gifs?api_key=${API_KEY}&ids=${ids}`;

  try {
    const promise = await fetch(query);
    const json = await promise.json();
    handler(json.data);
  } catch (error) {
    toastr.error(error.message);
  }
};

/**
 * Gets a favorite gif which id is stored in LocalStorage or a random gif.
 * Utilizes Giphy API Get GIF By Id Endpoint or Random Endpoint.
 *
 * @param {function} handler Function which will handle
 * the received Giphy API GIF object
 * and draw it to the interface
 */
const getFavorite = async (handler) => {
  const id = getLocalFavorite();
  if (id === null) {
    await getRandom(handler);
  } else {
    await getGifById(id, handler);
  }
};

/**
 * Uploads a gif file from local hard drive to Giphy Cloud.
 * Utilizes Giphy API Upload Endpoint.
 */
const uploadGif = async () => {
  const gif = prepareUploadRequest();
  const query = `${SERVER_UPLOAD}?api_key=${API_KEY}`;

  toastr.info('Uploading...');

  try {
    const response = await fetch(query, {
      method: 'POST',
      body: gif
    });
    const result = await response.json();
    const {
      id
    } = await result.data;
    setLocalUpload(id);
    toastr.success('GIF Uploaded.');
  } catch (error) {
    toastr.error(error.message);
  }
};

/**
 * Gets a list of uploaded by the user gifs.
 * IDs are taken from LocalStorage.
 * If no uploaded gifs for the user, gets a random gif
 * Utilizes Giphy API Get GIFs by ID Endpoint or Random Endpoint
 * @param {function} handler Function which will handle
 * the received {Promise<Array>} containing Giphy API GIF objects
 * and draw it to the interface
 */
const getUploads = async (handler) => {
  const ids = getLocalUploads();
  if (ids === null) {
    await getRandom(handler);
  } else {
    await getGifsById(ids, handler);
  }
};

/**
 * Gets a list of valid terms that complete the given search string.
 * Utilizes Giphy API Autocomplete Endpoint.
 * @param {string} searchString The string to search for
 * @param {function} handler Function which will handle
 * the received {Promise<Array>} containing Giphy API Term objects
 * and draw it to the interface
 */
const getAutoComplete = async (searchString, handler) => {
  /* eslint-disable max-len */
  const query = `${SERVER}/gifs/search/tags?api_key=${API_KEY}&q=${searchString}`;
  /* eslint-enable max-len */
  try {
    const promise = await fetch(query);
    const json = await promise.json();
    handler(json.data);
  } catch (error) {
    toastr.error(error.message);
  }
};

/**
 * Gets a list of valid terms that relate the given string.
 * Utilizes Giphy API Search Suggestions Endpoint.
 * @param {string} searchString The string to search for
 * @param {function} handler Function which will handle
 * the received {Promise<Array>} containing Giphy API Term objects
 * and draw it to the interface
 */
const getSearchSuggestions = async (searchString, handler) => {
  /* eslint-disable max-len */
  const query = `${SERVER}/tags/related/${searchString}?api_key=${API_KEY}&term=${searchString}`;
  /* eslint-enable max-len */
  try {
    const promise = await fetch(query);
    const json = await promise.json();
    handler(json.data);
  } catch (error) {
    toastr.error(error.message);
  }
};

/**
 * Gets a list of the most popular search terms.
 * Utilizes Giphy API Trending Search Terms Endpoint.
 * @param {function} handler Function which will handle
 * the received {Promise<Array>} containing String objects
 * and draw it to the interface
 */
const getTrendingSearches = async (handler) => {
  const query = `${SERVER}/trending/searches?api_key=${API_KEY}`;

  try {
    const promise = await fetch(query);
    const json = await promise.json();
    handler(json.data);
  } catch (error) {
    toastr.error(error.message);
  }
};

export {
  getSearch,
  getTrending,
  getRandom,
  getGifById,
  getGifsById,
  getFavorite,
  uploadGif,
  getUploads,
  getAutoComplete,
  getSearchSuggestions,
  getTrendingSearches,
}
