import * as services from './services.js';
import {
  viewUploads,
  viewTrending,
  setInfiniteScroll,
  initiateSearch,
  initiateUpload,
  drawFavorite,
  displayDetailsModal,
  loadSearchBoxSuggestions,
  setFavorite,
  favoriteRemove
} from './views.js';
import {
  copyLinkToClipboard
} from './utils.js';
import {
  showUpload,
  showTrending,
  uploadButton,
  infiniteScroll,
  searchGif,
  displayDetails,
  displaySearchSuggestions,
  addFavorite,
  removeFavorite,
  copyGifUrlAddress
} from './events.js';

/* Initialization */

// Populate the main GIF container
initiateSearch();
// Populate the Favorite GIF container
services.getFavorite(drawFavorite);

/* Iife to load all the event listeners
from events.js after the DOM is loaded*/
$(() => {
  showTrending(viewTrending);
  showUpload(viewUploads);
  uploadButton(initiateUpload);
  infiniteScroll(setInfiniteScroll);
  searchGif(initiateSearch);
  displayDetails(displayDetailsModal);
  displaySearchSuggestions(loadSearchBoxSuggestions);
  addFavorite(setFavorite)
  removeFavorite(favoriteRemove);
  copyGifUrlAddress(copyLinkToClipboard);
})
