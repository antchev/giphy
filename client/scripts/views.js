/* eslint-disable max-len */

import {
  getUploads,
  getFavorite,
  getTrending,
  getSearch,
  getSearchSuggestions,
  getTrendingSearches,
  uploadGif,
  getGifById,
  getAutoComplete
} from './services.js';
import {
  CONTAINER,
  CONTAINER_FAV,
  CONTAINER_SEARCH,
  DATE_FORMAT
} from './constants.js';
import {
  getCounter,
  resetCounter,
  setCounter,
  getPage,
  setPage,
  resetSearchField
} from './utils.js';
import {
  getLocalFavorite,
  setLocalFavorite,
  removeLocalFavorite
} from './localStorage.js';

/**
 * Showing the gifs.
 *
 *@param {Array} data Array with gifs details added in the main container with html.
 */
const showGifs = (data) => {
  const $div = $(CONTAINER);
  let counter = getCounter();
  let html = '';
  html += '<div class="col card-columns">';
  if (data instanceof Array) {
    data.map((gif) => {
      counter++;
      html +=
        `\n<div class="card" id="${gif.id}">
          <img data-id="${gif.id}" class="card-img-top card-image" data-toggle="modal" data-target="#exampleModal${gif.id}"
            src="${gif.images.fixed_width_downsampled.url}" alt="${gif.title}" title="Click to view Full size and Details">
        </a>
      </div>`;
    })
  } else {
    html +=
      `\n<div class="card" id="${data.id}">
        <img data-id="${data.id}" class="card-img-top card-image" data-toggle="modal" data-target="#exampleModal${data.id}"
          src="${data.images.fixed_width_downsampled.url}" alt="${data.title}" title="Click to view Full size and Details">
      </a>
    </div>`;
  }
  html += '\n</div>';
  $div.append(html);
  setCounter(counter);
}

/**
 * Draw the favorite gif.
 *
 * @param {object} gif's data added to main container with html
 */
const drawFavorite = (gif) => {
  const hasFav = getLocalFavorite('favorite') ? true : false;
  const $div = $(CONTAINER_FAV);
  $div.empty();
  let gifUrl = gif.images.original.url;
  if (gif.images.original.width > 500) {
    gifUrl = gif.images.fixed_width.url;
  }
  let html = '';
  let favGreeting = 'Set a favorite GIF to see here...';

  if (gif) {
    if (hasFav) {
      favGreeting = 'Best GIF Ever!';
    }
    html +=
      `<div class="card" id="${gif.id}">
        <h2>${favGreeting}</h2>
        <img data-id="${gif.id}" class="card-img-top card-image" data-toggle="modal" data-target="#exampleModal${gif.id}"
        src="${gifUrl}" alt="${gif.title}" title="Click to view Full size and Details">
    </div>`;
  }

  $div.append(html);
}

/**
 * Show trending suggestions.
 *
 * @param {object} data for search suggestions added in the main container with html
 */
const showSuggestions = async (data) => {
  const $div = $(CONTAINER);
  let html = '';
  html += '<div id="suggestions">';
  if (data instanceof Array) {
    data.map((suggestion) => {
      let suggName = '';
      if (typeof suggestion === 'object') {
        suggName = suggestion.name;
      } else {
        suggName = suggestion;
      }
      let trimmedSugg = suggName.replace(/ /g, '');
      html += `<a href="#" id="suggestion_${trimmedSugg}">
                <span class="suggestion-text">#${suggName}</span>
              </a>`;

      $(document).off('click', `#suggestion_${trimmedSugg}`).on('click', `#suggestion_${trimmedSugg}`, async () => {
        $('#search-term').val(suggName);
        await initiateSearch();
      });
    })
  }
  html += '</div>';
  $div.append(html);
}

/**
 * Show trending search box suggestions.
 *
 * @param {object} data for search box suggestions added with html
 */
const showSearchBoxSuggestions = (data) => {
  const $div = $(CONTAINER_SEARCH);
  $div.empty();
  let html = '';
  if (data instanceof Array) {
    data.map((suggestion) => {
      let suggName = '';
      if (typeof suggestion === 'object') {
        suggName = suggestion.name;
      } else {
        suggName = suggestion;
      }
      let trimmedSugg = suggName.replace(/[ '"]+/g, '');
      html += `<a class = "dropdown-item" href="#" id="suggestion_${trimmedSugg}">
                <span class="suggestion-text">${suggName}</span>
              </a>`;

      $(document).off('click', `#suggestion_${trimmedSugg}`).on('click', `#suggestion_${trimmedSugg}`, async () => {
        $('#search-term').val(suggName);
        $div.hide();
        initiateSearch();
      });
    })
  }
  $div.append(html);
}

/**
 * Showing the uploads added in the main container with html.
 *
 */
const viewUploads = async () => {
  resetSearchField();
  resetCounter();
  setPage('Uploads');
  const $div = $(CONTAINER);
  $div.empty();
  const html = `<div class="row">
                <div class="col-lg-12">
                  <h2>Uploads</h2>
                  <input id="upload-input" type="file" name="uploads" />
                  <button type="button" id="btn-upload" class="btn btn-info">Upload</button></br></br>
                </div>
              </div>`;
  $div.append(html);
  await getUploads(showGifs);
}

/**
 * Showing the trending.
 *
 */
const viewTrending = async () => {
  resetSearchField();
  resetCounter();
  setPage('Trending');
  const $div = $(CONTAINER);
  $div.empty();
  $div.append('<h2>Trending</h2>');
  const suggDiv = $('#suggestions');
  suggDiv.empty();
  await getTrendingSearches(showSuggestions);
  await getTrending(showGifs);
}

/**
 * Load search box trending suggestions.
 *
 */
const loadSearchBoxSuggestions = async () => {
  resetCounter();
  setPage('Search');
  const $div = $(CONTAINER_SEARCH);
  $div.show();
  const searchTerm = $('#search-term').val();
  if (searchTerm === '') {
    await getTrendingSearches(showSearchBoxSuggestions);
  } else {
    await getAutoComplete(searchTerm, showSearchBoxSuggestions);
  }
}

/**
 * Initiate the uploading of the file.
 */
const initiateUpload = async () => {
  await uploadGif();
  viewUploads();
}

/**
 * Initiating the search.
 *
 */
const initiateSearch = async () => {
  resetCounter();
  setPage('Search');
  const $div = $(CONTAINER);
  $div.empty();
  const suggDiv = $('#suggestions');
  suggDiv.empty();
  const searchSuggDiv = $(CONTAINER_SEARCH);
  searchSuggDiv.empty();
  searchSuggDiv.hide();

  let searchTerm = $('#search-term').val();
  let drawSuggestions = true;

  if (searchTerm === '') {
    const randomChar = String.fromCharCode(97 + Math.floor(Math.random() * 26));
    searchTerm = randomChar;
    drawSuggestions = false;
  }

  if (drawSuggestions) {
    $div.append(`<h2>Search results for "${searchTerm}"</h2>`);
    await getSearchSuggestions(searchTerm, showSuggestions);
  }

  const offset = getCounter();
  await getSearch(showGifs, offset);
}
/**
 * Set infinite scroll.
 *
 */
const setInfiniteScroll = async () => {
  const scrollHeight = $(document).height();
  const scrollPos = $(window).height() + $(window).scrollTop();
  if ((scrollHeight - scrollPos) / scrollHeight === 0) {
    const page = getPage();
    const offset = getCounter();
    if (page === 'Search') {
      await getSearch(showGifs, offset);
    } else if (page === 'Trending') {
      await getTrending(showGifs, offset);
    }
  }
}
/**
 * Displaying the details of the bootstrap modal.
 *
 *@param {Event} event The properties of the event.
 */
const displayDetailsModal = async (event) => {
  const id = event.target.attributes['data-id'].value;
  const data = await getGifById(id, constructModal);
  const $div = $(CONTAINER);
  $div.append(data);
  $('#exampleModal' + id).modal('show');
}

/**
 * Displaying the details of the modal form.
 *
 *@param {Object} data The properties of the object.
 *@return {String} The html elements.
 */
const constructModal = (data) => {
  const isFavorite = getLocalFavorite() === data.id;
  return `<div class="modal fade in" id="exampleModal${data.id}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <div>
                    <p class="modal-header">${data.title || 'No name'}</p>
                    <div class="button-container modal-icons">
                      <div>
                        <i class="modal-icon fa fa-heart fa-2x ${isFavorite? 'show-like' : 'hide-like'} liked-click" id="liked-${data.id}" aria-hidden="true" data-id="${data.id}"></i> 
                        <i class="modal-icon fa fa-heart-o fa-2x ${isFavorite? 'hide-like': 'show-like'} not-liked-click" id="not-liked-${data.id}" data-id="${data.id}" aria-hidden="true"></i>
                      </div>
                      <div>
                        <i class="modal-icon fa fa-chain fa-2x get-link" id="${data.url}"></i>
                      </div>
                    </div>
                  </div>
                <div>
                <div class="modal-body">
                  <a href="${data.url}" target="_blank" title="Open on Giphy.com">
                    <img src="${data.images.original.url}" alt="${data.title}" />
                  </a>
                  <p class="modal-info" id="user-name">Username: ${data.username || 'No user info'}</p>
                  <p class="modal-info">Date: ${moment(data.import_datetime).format(DATE_FORMAT)}</p>
                </div>
              </div>
            </div>
          </div>
            `;
}

/**
 * Set the favorite.
 *
 *@param {Event} event The properties of the event.
 */
const setFavorite = (event) => {
  const id = event.target.attributes['data-id'].value;
  setLocalFavorite(id);
  $('#liked-' + id).addClass('show-like');
  $('#liked-' + id).removeClass('hide-like');
  $('#not-liked-' + id).addClass('hide-like');
  $('#not-liked-' + id).removeClass('show-like');
  getFavorite(drawFavorite);
  toastr.success('Favorite GIF selected');
}

/**
 * Remove favorite gif.
 *
 *@param {Event} event The properties of the event.
 */
const favoriteRemove = (event) => {
  const id = event.target.attributes['data-id'].value;
  removeLocalFavorite();
  $('#liked-' + id).addClass('hide-like');
  $('#liked-' + id).removeClass('show-like');
  $('#not-liked-' + id).addClass('show-like');
  $('#not-liked-' + id).removeClass('hide-like');
  getFavorite(drawFavorite);
  toastr.info('Favorite GIF removed');
}


export {
  showGifs,
  viewUploads,
  viewTrending,
  setInfiniteScroll,
  initiateSearch,
  initiateUpload,
  drawFavorite,
  displayDetailsModal,
  showSearchBoxSuggestions,
  loadSearchBoxSuggestions,
  setFavorite,
  favoriteRemove
}
