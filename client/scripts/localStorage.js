/**
 * Get item from local storage.
 *
 * @return {function} Accepting a key which value
 * to be get from the Local Storage.
 */
export const getLocalFavorite = () => {
  return localStorage.getItem('favorite');
}
/**
 * Get item from local storage.
 *
 * @return {Function} Accepting the key which value
 * to be get from the Local Storage.
 */
export const getLocalUploads = () => {
  return localStorage.getItem('uploads');
}
/**
 * Set item to the Local Storage.
 *
 * @param  {String} id The value to be set in the Local Storage.
 */
export const setLocalFavorite = (id) => {
  localStorage.setItem('favorite', id);
};
/**
 * Set item to the Local Storage.
 *
 * @param  {String} id The value to be set in the Local Storage.
 */
export const setLocalUpload = (id) => {
  let uploads = localStorage.getItem('uploads') || '';
  uploads = uploads ? uploads + ',' + id : id;
  localStorage.setItem('uploads', uploads);
}
/**
 * Remove item from the Local Storage.
 *
 */
export const removeLocalFavorite = () => {
  localStorage.removeItem('favorite');
};
