const API_KEY = 'MnlLmnWbJ0oYRd3NcPN8blMfuwwMHNPQ';
const SERVER = 'https://api.giphy.com/v1';
const SERVER_UPLOAD = 'https://upload.giphy.com/v1/gifs';
const DATE_FORMAT = 'Do of MMMM, YYYY';
const LIMIT = 24;
const CONTAINER = $('#main-container');
const CONTAINER_FAV = $('#favorite-container');
const CONTAINER_SEARCH = $('#suggestions-dropdown-list');

export { API_KEY, SERVER, SERVER_UPLOAD,
  DATE_FORMAT, LIMIT, CONTAINER, CONTAINER_FAV,
  CONTAINER_SEARCH }
